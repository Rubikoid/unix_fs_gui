package GUI;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.util.Set;
import java.util.prefs.Preferences;

/**
 * Created by Rubikoid on 16.02.2017.
 */
public class Settings {
	private static Preferences userPref = Preferences.userNodeForPackage(Settings.class);

	@FXML
	private TextField port;
	@FXML
	private TextField save;
	@FXML
	private TextField cat;

	public static int getPort(){
		int portI = Settings.userPref.getInt("port", 6660);
		return portI;
	}

	public void load(){
		this.port.setText(Settings.getPort()+"");
	}

	@FXML
	private void setPort() {
		int portI;
		try{
			portI = Integer.parseInt(port.getText());
		}
		catch(NumberFormatException ex){
			portI = 6660;
		}
		Settings.userPref.putInt("port", portI);
	}

	@FXML
	private void setSave() {
		//Settings.saveString = save.textProperty().getValue();
	}

	@FXML
	private void setCat() {
		//Settings.catString = cat.textProperty().getValue();
	}

}
