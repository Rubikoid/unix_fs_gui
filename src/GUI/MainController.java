package GUI;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javafx.scene.input.KeyEvent;

import javax.swing.text.StyledDocument;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Rubikoid on 16.02.2017.
 */
public class MainController {
	private Connection connection;
	private String lastCommand = "";
	private String webData = "";
	private final String htmlTemplate =
			"<!doctype html>" +
			"<html>" +
			"<head>" +
			"  <meta charset=\"utf-8\">" +
			"  <script> function toBottom(){window.scrollTo(0, document.body.scrollHeight);} </script>" +
			"</head>" +
			"<body onload='toBottom()'>" + //
			"<pre style=\"word-wrap:break-word;\">" + //
			"${code}" +
			"</pre>" +
			"</body>" +
			"</html>";
	@FXML
	private TextField fileNameCtrl;
	@FXML
	private TextField cmd;
	@FXML
	private WebView console;
	@FXML
	private TextArea file;

	public MainController() {

	}

	@FXML
	private void settingsShow() {
		Parent root;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("settings.fxml"));
			root = loader.load();
			Stage stage = new Stage();
			stage.setTitle("Settings");
			stage.setScene(new Scene(root, 450, 200));
			((Settings)loader.getController()).load();
			stage.show();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	void close(){
		try{this.connection.exit(false);}
		catch(Exception ex){}
	}

	@FXML
	private void keyPressed(KeyEvent event){
		//System.out.print(event.getCode() + ",");
	}

	@FXML
	private void connectToServer() {
		this.console.setPrefSize(Region.USE_COMPUTED_SIZE,Region.USE_COMPUTED_SIZE);
		if(this.connection == null || this.connection.socket == null || this.connection.socket.isClosed()) {
			this.connection = new Connection(this.cmd, this);
		}
	}

	@FXML
	private void saveFile(){
		this.lastCommand = "ef " + this.fileNameCtrl.getText() + " " + this.file.getText();
		System.out.println(this.lastCommand);
		this.appendToConsole("ef " + this.fileNameCtrl.getText() + " ****" + "\n");
		this.sendCommand(this.lastCommand);
	}

	@FXML
	private void sendCommand(){
			this.lastCommand = this.cmd.getText();
			System.out.println(this.lastCommand);
			this.appendToConsole(this.lastCommand+"\n");
			this.sendCommand(this.lastCommand);
			this.cmd.setText("");
	}

	private void sendCommand(String command){
		try {
			this.connection.sendMessage(this.lastCommand);
		}
		catch(Exception ex){
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Exception");
			alert.setHeaderText(null);
			alert.setContentText(ex.toString()+"\nPossible reason: client not connected to server");
			alert.showAndWait();
		}
	}

	void receiveData(String data){
		if(!data.matches(".*@.*#.*>") && this.lastCommand.contains("cat")){
			this.fileNameCtrl.setText(this.lastCommand.split(" ")[1]);
			this.file.setText(data);
		}
	}

	void appendToConsole(String text) {
		this.webData += text;
		this.console.getEngine().loadContent(generateTemplate());
	}

	private String generateTemplate() {
		return this.htmlTemplate.replace("${code}", this.webData.replace("\n", "<br>"));
	}
}
