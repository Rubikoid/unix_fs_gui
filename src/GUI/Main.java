package GUI;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
//
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("UnixFSEmu Client");
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add(getClass().getResource("main.fxml").toExternalForm());
        primaryStage.setScene(scene);
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				((MainController)loader.getController()).close();
			}
		});
        primaryStage.show();
    }


    public static void main(String[] args) {
    	launch(args);
    }
}
