package GUI;

import java.io.IOException;
import java.net.Socket;

import javafx.scene.paint.Color;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.application.Platform;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * Created by Rubikoid on 16.02.2017.
 */
public class Connection extends Thread {
	Socket socket;
	public TextField cmd;
	private MainController ctrl;

	public Connection(TextField cmd, MainController ctrl){
		try {
			this.socket = new Socket("89.179.245.232", Settings.getPort());
			//this.socket = new Socket("127.0.0.1", 6600);
		}
		catch(IOException ex){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Exception");
			alert.setHeaderText(null);
			alert.setContentText(ex.toString() + "\nPossible reasons: wrong port, server down");
			alert.showAndWait();
		}
		this.cmd = cmd;
		this.ctrl = ctrl;
		setDaemon(false);
		setPriority(NORM_PRIORITY);
		start();
	}

	public void sendMessage(String data) throws IOException {
		socket.getOutputStream().write(data.getBytes());
	}

	public String recive() throws IOException {
		byte buf[] = new byte[64 * 1024];
		int r = socket.getInputStream().read(buf);
		if(r == - 1) return null;
		return new String(buf, 0, r).replace("\r", "");
	}

	public void run() {
		try {
			String data = "";
			while(!this.isInterrupted()) {
				data = recive();
				if(data == null) break;
				this.ctrl.receiveData(data);
				String finalData = data;
				Platform.runLater(new Runnable(){
					@Override public void run() {
						if(finalData.contains("\u001B")) parseColoredText(finalData);
						else ctrl.appendToConsole(finalData);
					}
				});
			}
			this.exit(false);
		}
		catch(Exception e) {
			if(!this.isInterrupted()) {
				System.out.println("Error " + e);
				Platform.runLater(() -> {
						ctrl.appendToConsole("Disconnected from server\n");
					});
				try{this.socket.close();}
				catch(IOException ex){}
			}
		} // вывод исключений
	}

	private void parseColoredText(String finalData){
		//finalData = "_"+finalData;
		StringBuilder normalBuilder = new StringBuilder();
		for(int i=0;i<finalData.length();i++) {
			if(finalData.charAt(i) == '\u001B') {
				StringBuilder colorBuilder = new StringBuilder();
				char chatAt = finalData.charAt(i);
				while(chatAt != 'm') {
					i++;
					chatAt = finalData.charAt(i);
				}
				while(chatAt != '\u001B'){
					i++;
					chatAt = finalData.charAt(i);
					if(chatAt != '\u001B') colorBuilder.append(chatAt);
				}
				while(chatAt != 'm') {
					i++;
					chatAt = finalData.charAt(i);
				}
				colorBuilder.insert(0, "<font color=\"blue\">");
				colorBuilder.append("</font>");
				String coloredString = new String(colorBuilder);
				String normalString = new String(normalBuilder);
				if(!normalString.equals("")) ctrl.appendToConsole(normalString);
				ctrl.appendToConsole(coloredString);
				normalBuilder = new StringBuilder();
			}
			else normalBuilder.append(finalData.charAt(i));
		}
		String normalString = new String(normalBuilder);
		if(!normalString.equals("")) ctrl.appendToConsole(normalString);
	}

	public void exit(boolean connectionClosed) throws IOException {
		this.interrupt();
		if(!connectionClosed) sendMessage("exit");
		socket.close();
	}
}
